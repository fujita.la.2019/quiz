<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>結果</title>
</head>
<body>
<h1>結果</h1>
<p>${ result }</p>

<c:if test="${ result != '正解' }">
<p>正解は${ questions[questionNumber].answer }</p>
</c:if>
<a href="/quiz/home?next=${ questionNumber + 1 }">次へ</a>
</body>
</html>