<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>クイズ</title>
</head>
<body>
<h1>問題</h1>
<p>現在のスコア：${score}</p>
<p>第${questionNumber + 1}問 ${questions[questionNumber].question}</p>
<ul>
	<c:forEach var="choice" items="${questions[questionNumber].choices}">
		<li><a href="/quiz/result?answer=<c:out value="${choice}" />"><c:out value="${choice}" /></a></li>
	</c:forEach>
</ul>
</body>
</html>