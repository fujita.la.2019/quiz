package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Question {
	/**
	 * 問題文
	 */
	private String question;

	/**
	 * 選択肢
	 */
	private List<String> choices;

	/**
	 * 答え
	 */
	private String answer;

	public static class QuestionBuilder {
		private String question;
		private String[] choices;
		private String answer;

		public Builder question(String question) {
			this.question = question;
			return this;
		}

		public Builder choices(String...choices) {
			this.choices = choices;
			return this;
		}

		public Builder answer(String answer) {
			this.answer = answer;
			return this;
		}

		public Question build() {
            return new Question(this);
        }
	}

	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public List<String> getChoices() {
		return choices;
	}
	public void setChoices(List<String> choices) {
		this.choices = choices;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * コンストラクタ
	 */
	private Question(Builder builder) {
		this.question = builder.question;
		this.answer = builder.answer;
		this.setChoices(new ArrayList<>());
		this.getChoices().add(builder.answer)	;
		for(String option : choices) {
			this.getChoices().add(option);
		}
	}

	/**
	 * 選択肢の順番をシャッフルする
	 */
	public void shuffle() {
		Collections.shuffle(this.getChoices());
	}

	/**
	 * 正解かどうかの判定
	 * @param answer 選択した選択肢のテキスト
	 * @return 正解ならtrue、不正解ならfalse
	 */
	public boolean isAnswer(String answer) {
		if(this.getAnswer().equals(answer)) {
			return true;
		}else {
			return false;
		}
	}
}
