package listener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import model.Question;

/**
 * Application Lifecycle Listener implementation class SessionListener
 *
 */
@WebListener
public class SessionListener implements HttpSessionListener {

    /**
     * Default constructor.
     */
    public SessionListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent arg0)  {
    	//問題の作成
    	//TODO JSONから読み込む方法に変更
    	List<Question> questions = new ArrayList<>();
    	Question question;

    	question = new Question("ウェブサイトの骨組みを作るのは？","HTML","CSS","JavaScript");
    	question.shuffle();
    	questions.add(question);

    	question = new Question("idを指定してDOMにアクセスするのは？","getElementById()","getElementsById()","getElementByClassName()","getElementsByClassName()");
    	question.shuffle();
    	questions.add(question);

    	question = new Question("講師の名前は？","井野","猪野","入野","生野");
    	question.shuffle();
    	questions.add(question);

    	Collections.shuffle(questions);

    	//セッションスコープに格納
    	HttpSession session = arg0.getSession();
    	session.setAttribute("questions", questions);
    	session.setAttribute("score", 0);
		session.setAttribute("questionNumber", 0);
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent arg0)  {
         // TODO Auto-generated method stub
    }

}
