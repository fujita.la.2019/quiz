package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Question;

/**
 * Servlet implementation class Result
 */
@WebServlet("/result")
public class Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Result() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//回答を取得
		String answer = request.getParameter("answer");

		//現在の問題番号を取得
		HttpSession session = request.getSession();
		int questionNumber = Integer.parseInt(session.getAttribute("questionNumber").toString());

		//問題を取得
		List<Question> questions = (List<Question>)session.getAttribute("questions");
		Question question = questions.get(questionNumber);

		//正誤判定
		if(question.isAnswer(answer)) {
			request.setAttribute("result", "正解");
			int score = Integer.parseInt(session.getAttribute("score").toString()) + 1;
			session.setAttribute("score", score);
		}else {
			request.setAttribute("result", "不正解");
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/result.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
